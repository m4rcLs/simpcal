import moment from "moment-timezone";
const now = moment();
now.subtract(now.get("minutes"), "minutes");
export const newEventDefault = {
  title: "",
  timezone: moment.tz.guess(),
  startDate: now.format("YYYY-MM-DD"),
  startTime: now.format("HH:mm"),
  endDate: now.format("YYYY-MM-DD"),
  endTime: now
    .clone()
    .add(2, "hours")
    .format("HH:mm"),
  color: "#FF0000FF"
};
