import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import DatetimePicker from "vuetify-datetime-picker";
import store from "./store";
import { v4 } from "uuid";

Vue.config.productionTip = false;

Vue.use(DatetimePicker);
Vue.prototype.$uuid = v4;
Vue.prototype.$backendPort = 54009;

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
