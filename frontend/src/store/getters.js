import moment from "moment-timezone";

export default {
  getLocalizedEvents(state) {
    const events = [];

    state.events.forEach(e => {
      const start = moment
        .unix(e.start)
        .tz(state.currentTimezone)
        .format("YYYY-MM-DD HH:mm");
      const end = moment
        .unix(e.end)
        .tz(state.currentTimezone)
        .format("YYYY-MM-DD HH:mm");
      const event = {
        ...e,
        start,
        end
      };
      events.push(event);
    });

    return events;
  }
};
