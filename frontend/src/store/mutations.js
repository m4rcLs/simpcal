import { newEventDefault } from "../shared/defaults";

export default {
  updateEvents(state, events) {
    state.events = events;
  },
  addEvent(state, event) {
    state.events.push(event);
    state.newEvent = { ...newEventDefault };
  },
  deleteEvent(state, eventId) {
    state.events = state.events.filter(event => event.id !== eventId);
  },
  updateCurrentTimezone(state, timezone) {
    state.currentTimezone = timezone;
  },
  updateNewEvent(state, newEvent) {
    Object.keys(newEvent).forEach(key => {
      state.newEvent[key] = newEvent[key];
    });
  }
};
