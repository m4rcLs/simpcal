import axios from "axios";

const backendUrl = "http://localhost:54009";
export default {
  loadEvents(store) {
    axios.get(`${backendUrl}/events`).then(response => {
      store.commit("updateEvents", response.data.events);
    });
  },
  addEvent(store, event) {
    axios
      .post(`${backendUrl}/events`, {
        events: [event]
      })
      .then(() => {
        store.commit("addEvent", event);
      });
  },
  deleteEvent(store, eventId) {
    axios
      .delete(`${backendUrl}/events?ids=${eventId}`, {
        events: [event]
      })
      .then(() => {
        store.commit("deleteEvent", eventId);
      });
  }
};
