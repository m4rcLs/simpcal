import moment from "moment-timezone";
import { newEventDefault } from "../shared/defaults";

export default {
  localTimezone: "Europe/Berlin",
  currentTimezone: "Europe/Berlin",
  timezoneOptions: moment.tz.names(),
  events: [],
  newEvent: {
    ...newEventDefault
  }
};
