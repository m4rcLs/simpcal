# simpCal - A simple calendar app

simpCal is a single calendar app based on Go and Electron, using the great [astilectron](github.com/asticode/go-astilectron) wrapper library

## Build

### 1. Build the frontend

Switch to the frontend directory and build the frontend (preferably with yarn)

```sh
cd frontend && yarn build
```

### 2. Build the backend

The backend is build with Go simply by running:

```
go build
```

For a production build you should set the `windowsgui` flag, so that the executable does not create a `cmd` window. (only needed on Windows)

```
go build -ldflags -H=windowsgui
```

### 3. Move the frontend relative to the executable

The backend will start a HTTP server that statically serves the `./resources/app` folder.  
Make sure that the build output of the frontend is located in this folder relative to the executable path.  
