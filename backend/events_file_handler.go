package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// An Event is a certain timed happening with a start and end time
type Event struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Start int64  `json:"start"`
	End   int64  `json:"end"`
	Color string `json:"color"`
}

// Events is a collection of different Events.
type Events struct {
	Events []*Event `json:"events"`
}

// EventFileHandler holds all the functions regarding events from a JSON file.
type EventFileHandler struct {
	filePath       string
	events         *Events
	saveEventsChan chan (bool)
}

// NewEventFileHandler creates a new EventFileHandler with the specified filePath
func NewEventFileHandler(filePath string) *EventFileHandler {
	handler := &EventFileHandler{
		filePath:       filePath,
		saveEventsChan: make(chan (bool), 10),
		events: &Events{
			Events: []*Event{},
		},
	}

	go handler.listenForSaveRequests()
	return handler

}

func (e *EventFileHandler) listenForSaveRequests() {
	for {
		<-e.saveEventsChan
		if err := e.SaveEvents(); err != nil {
			panic(fmt.Sprintf("Could not save Events: %v", err))
		}

	}
}

// LoadEvents opens the file with the given filePath and tries to parse it as JSON into Events.
func (e *EventFileHandler) LoadEvents() (err error) {
	events := &Events{}
	fileContents, err := ioutil.ReadFile(e.filePath)
	if err != nil {
		return
	}
	json.Unmarshal(fileContents, &events)

	e.events = events

	return
}

// SaveEvents opens the file with the given filePath and writes the given Events in JSON format.
func (e *EventFileHandler) SaveEvents() (err error) {
	content, err := json.Marshal(e.events)

	if err != nil {
		return
	}

	err = ioutil.WriteFile(e.filePath, content, 0644)

	return
}

// CreateEvents creates the given events, if they do not already exist and updates existing ones.
func (e *EventFileHandler) CreateEvents(events *Events) {
	if events == nil || len(events.Events) == 0 {
		return
	}
	var alreadyExists bool

	for _, event := range events.Events {
		alreadyExists = false
		for i, existingEvent := range e.events.Events {
			if event.ID == existingEvent.ID {
				e.events.Events[i] = event
				alreadyExists = true
				break
			}
		}

		if !alreadyExists {
			e.events.Events = append(e.events.Events, event)
		}
	}
	e.saveEventsChan <- true
	return
}

// GetEvents returns all existing events.
func (e *EventFileHandler) GetEvents() (*Events, error) {
	if e.events == nil || len(e.events.Events) == 0 {
		err := e.LoadEvents()
		if err != nil {
			return e.events, err
		}
	}

	return e.events, nil
}

// GetEvent returns the event for a given ID. Returns nil if the event does not exist.
func (e *EventFileHandler) GetEvent(id string) (event *Event) {
	if id == "" || e.events == nil || len(e.events.Events) == 0 {
		return
	}

	for _, existingEvent := range e.events.Events {
		if id == existingEvent.ID {
			event = existingEvent
			break
		}
	}

	return
}

// DeleteEvent deletes and returns the event for a given ID. Returns nil if the event does not exist.
func (e *EventFileHandler) DeleteEvent(id string) (event *Event) {
	if id == "" || e.events == nil || len(e.events.Events) == 0 {
		return
	}

	eventCount := len(e.events.Events)
	for i, existingEvent := range e.events.Events {
		if id == existingEvent.ID {
			event = existingEvent
			// This is faster than re-slicing: append(e.events.Events[:i], e.events.Events[i+1:]...)
			// but it messes with the order of the slice. Actually we don't mind this here ...
			e.events.Events[i] = e.events.Events[eventCount-1]
			e.events.Events = e.events.Events[:eventCount-1]
			break
		}
	}
	e.saveEventsChan <- true
	return
}
