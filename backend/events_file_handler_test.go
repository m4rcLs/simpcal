package main

import (
	"os"
	"reflect"
	"testing"
	"time"
)

func TestGetEvents(t *testing.T) {
	expectedEvents := &Events{
		Events: []*Event{
			&Event{
				ID:    "38de406b-1d44-4b4c-afc1-5b56ab92b213",
				Name:  "Red Event",
				Start: 1611486596,
				End:   1611490196,
				Color: "#ff0000",
			},
			&Event{
				ID:    "f8148272-d718-4518-b8a3-511070830e68",
				Name:  "Green Event",
				Start: 1611586596,
				End:   1611590196,
				Color: "#00ff00",
			},
			&Event{
				ID:    "01713b21-b2b0-48b5-950d-ec8fad27936e",
				Name:  "Blue Event",
				Start: 1611686596,
				End:   1611690196,
				Color: "#0000ff",
			},
		},
	}
	filePath := "events.json"
	handler := NewEventFileHandler(filePath)

	events, err := handler.GetEvents()
	if err != nil {
		t.Errorf("Could not get events from %s, even though it should", filePath)
	}

	if len(events.Events) != 3 {
		t.Errorf("Read %s resulted in %d events, wanted 3", filePath, len(events.Events))
	}

	for i, event := range events.Events {
		if same := reflect.DeepEqual(event, expectedEvents.Events[i]); !same {
			t.Errorf("%s and %s are not the same events", event.Name, expectedEvents.Events[i].Name)
		}
	}

	nonExistingFileHandler := NewEventFileHandler("nonExistingFile")

	_, err = nonExistingFileHandler.GetEvents()

	if err == nil {
		t.Error("Reading a non existing file did not return an error, though it should")
	}

}

func TestCreateEvents(t *testing.T) {
	expectedEvents := &Events{
		Events: []*Event{
			&Event{
				ID:    "38de406b-1d44-4b4c-afc1-5b56ab92b213",
				Name:  "Red Event",
				Start: 1611486596,
				End:   1611490196,
				Color: "#ff0000",
			},
			&Event{
				ID:    "f8148272-d718-4518-b8a3-511070830e68",
				Name:  "Green Event",
				Start: 1611586596,
				End:   1611590196,
				Color: "#00ff00",
			},
			&Event{
				ID:    "01713b21-b2b0-48b5-950d-ec8fad27936e",
				Name:  "Blue Event",
				Start: 1611686596,
				End:   1611690196,
				Color: "#0000ff",
			},
		},
	}
	filePath := "writeTest.json"
	handler := NewEventFileHandler(filePath)

	handler.CreateEvents(expectedEvents)
	// We wait because saving Events is asynchronous
	time.Sleep(100 * time.Millisecond)

	events, err := handler.GetEvents()

	if err != nil {
		t.Errorf("Could not get created events")
	}

	for i, event := range events.Events {
		if same := reflect.DeepEqual(event, expectedEvents.Events[i]); !same {
			t.Errorf("%s and %s are not the same events", event.Name, expectedEvents.Events[i].Name)
		}
	}

	// Check if createEvents was persisted
	handler.LoadEvents()
	for i, event := range events.Events {
		if same := reflect.DeepEqual(event, expectedEvents.Events[i]); !same {
			t.Errorf("%s and %s are not the same events", event.Name, expectedEvents.Events[i].Name)
		}
	}

	// Check if overwrite functions correctly
	expectedEvents.Events[0].ID = "1234"
	expectedEvents.Events[1].Name = "Not Green Event"
	expectedEvents.Events[2].Color = "#000000"

	handler.CreateEvents(expectedEvents)
	events, err = handler.GetEvents()
	if err != nil {
		t.Errorf("Could not get created events")
	}

	if len(events.Events) != 4 {
		t.Errorf("Length is %d, should be 4 after seconde CreateEvents", len(events.Events))
		return
	}

	if expectedEvents.Events[0].ID != events.Events[3].ID {
		t.Errorf("New event with ID %s was not created", expectedEvents.Events[0].ID)
	}

	if expectedEvents.Events[1].Name != events.Events[1].Name {
		t.Errorf("Name is %s but should be %s now", events.Events[1].Name, expectedEvents.Events[1].Name)
	}

	if expectedEvents.Events[2].Color != events.Events[2].Color {
		t.Errorf("Color is %s but should be %s now", events.Events[2].Color, expectedEvents.Events[2].Color)
	}

	handler = NewEventFileHandler("ThisFileShouldNotExist")

	handler.CreateEvents(nil)
	handler.CreateEvents(&Events{
		Events: []*Event{},
	})

	// Because SaveEvent is asynchronous, we wait here for the file to be written
	time.Sleep(100 * time.Millisecond)

	if _, err := os.Stat("ThisFileShouldNotExist"); err == nil {
		t.Errorf("Events file was written even though it should not been")
	}

	if err = os.Remove(filePath); err != nil {
		t.Errorf("Could not remove %s", filePath)
	}

}

func TestGetEvent(t *testing.T) {
	expectedEvents := &Events{
		Events: []*Event{
			&Event{
				ID:    "38de406b-1d44-4b4c-afc1-5b56ab92b213",
				Name:  "Red Event",
				Start: 1611486596,
				End:   1611490196,
				Color: "#ff0000",
			},
			&Event{
				ID:    "f8148272-d718-4518-b8a3-511070830e68",
				Name:  "Green Event",
				Start: 1611586596,
				End:   1611590196,
				Color: "#00ff00",
			},
			&Event{
				ID:    "01713b21-b2b0-48b5-950d-ec8fad27936e",
				Name:  "Blue Event",
				Start: 1611686596,
				End:   1611690196,
				Color: "#0000ff",
			},
		},
	}

	filePath := "getTest.json"
	handler := NewEventFileHandler(filePath)

	handler.CreateEvents(expectedEvents)
	event := handler.GetEvent("f8148272-d718-4518-b8a3-511070830e68")

	if same := reflect.DeepEqual(event, expectedEvents.Events[1]); !same {
		t.Errorf("Did not get the correct event with GetEvent")
	}

	if event = handler.GetEvent(""); event != nil {
		t.Errorf("Got an event with no id")
	}

	// Because SaveEvent is asynchronous, we wait here for the file to be written
	time.Sleep(100 * time.Millisecond)
	if err := os.Remove(filePath); err != nil {
		t.Errorf("Could not remove %s", filePath)
	}
}

func TestDeleteEvent(t *testing.T) {
	expectedEvents := &Events{
		Events: []*Event{
			&Event{
				ID:    "38de406b-1d44-4b4c-afc1-5b56ab92b213",
				Name:  "Red Event",
				Start: 1611486596,
				End:   1611490196,
				Color: "#ff0000",
			},
			&Event{
				ID:    "f8148272-d718-4518-b8a3-511070830e68",
				Name:  "Green Event",
				Start: 1611586596,
				End:   1611590196,
				Color: "#00ff00",
			},
			&Event{
				ID:    "01713b21-b2b0-48b5-950d-ec8fad27936e",
				Name:  "Blue Event",
				Start: 1611686596,
				End:   1611690196,
				Color: "#0000ff",
			},
		},
	}

	filePath := "deleteTest.json"
	handler := NewEventFileHandler(filePath)
	eventID := "f8148272-d718-4518-b8a3-511070830e68"
	event := handler.DeleteEvent(eventID)
	if event != nil {
		t.Errorf("Deleted event before even creating it")
	}

	handler.CreateEvents(expectedEvents)
	// Because SaveEvent is asynchronous, we wait here for the file to be written
	time.Sleep(100 * time.Millisecond)

	event = handler.DeleteEvent(eventID)
	if same := reflect.DeepEqual(event, expectedEvents.Events[1]); !same {
		t.Errorf("Did not delete the correct event")
	}

	if event = handler.GetEvent(eventID); event != nil {
		t.Errorf("Event was not deleted")
	}

	handler.LoadEvents()
	time.Sleep(100 * time.Millisecond)

	if event = handler.GetEvent(eventID); event != nil {
		t.Errorf("Deletion was not persisted")
	}

	if event = handler.DeleteEvent(""); event != nil {
		t.Errorf("Deleted event without giving an ID")
	}

	// Because SaveEvent is asynchronous, we wait here for the file to be written
	time.Sleep(100 * time.Millisecond)
	if err := os.Remove(filePath); err != nil {
		t.Errorf("Could not remove %s", filePath)
	}
}
