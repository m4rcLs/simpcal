package main

// EventHandler holds all the functions regarding events.
type EventHandler interface {
	LoadEvents() error
	SaveEvents() error
	CreateEvents(events *Events)
	DeleteEvent(id string) *Event
	GetEvent(id string) *Event
	GetEvents() (*Events, error)
}
