module gitlab.com/m4rcLs/simpcal

go 1.15

require (
	github.com/asticode/go-astikit v0.17.0
	github.com/asticode/go-astilectron v0.22.3
)
