package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/asticode/go-astikit"
	"github.com/asticode/go-astilectron"
)

const defaultPort = 54009
const defaultFilePath = "events.json"

var corsEnabled = false

var handler EventHandler

func eventsHandler(w http.ResponseWriter, r *http.Request) {
	// enable CORS
	if corsEnabled {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
	}

	switch r.Method {
	case "GET":
		events, _ := handler.GetEvents()
		content, _ := json.Marshal(events)
		w.Header().Set("Content-Type", "application/json")
		w.Write(content)
	case "DELETE":
		ids := strings.Split(r.URL.Query()["ids"][0], ",")
		deletedEvents := []*Event{}
		for _, id := range ids {
			deletedEvent := handler.DeleteEvent(id)
			if deletedEvent != nil {
				deletedEvents = append(deletedEvents, deletedEvent)
			}
		}
		content, _ := json.Marshal(deletedEvents)
		w.Header().Set("Content-Type", "application/json")
		w.Write(content)
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Body was unprocessable"))
			return
		}
		var events Events
		err = json.Unmarshal(body, &events)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Body did not contain the valid JSON schema"))
			return
		}
		handler.CreateEvents(&events)
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
		return
	case "OPTIONS":
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		log.Printf("%v", r.URL.Query())
		return
	}

}

func startServer(port *int) {
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}

func main() {
	log.Print("simpCal Backend")

	filePath := flag.String("event-file", defaultFilePath, "File path for the events file in JSON format")
	port := flag.Int("port", defaultPort, "Port on which the HTTP server listens")
	debug := flag.Bool("debug", false, "Debug mode")
	flag.BoolVar(&corsEnabled, "cors", false, "Whether to enable CORS or not")
	flag.Parse()

	log.Print("Creating new EventFileHandler")
	log.Printf("Event file: %s", *filePath)
	handler = NewEventFileHandler(*filePath)
	log.Printf("Load events")
	handler.LoadEvents()

	log.Print("Setup HTTP handler")
	http.HandleFunc("/events", eventsHandler)
	if corsEnabled {
		log.Println("CORS enabled")
	}
	log.Print("Serve frontend")
	fs := http.FileServer(http.Dir("./resources/app"))
	http.Handle("/", fs)

	serverURL := fmt.Sprintf("http://localhost:%d", *port)

	log.Printf("Listen on *:%d ...", *port)
	go startServer(port)

	icon := "resources/icon.png"
	// Initialize astilectron
	var a, _ = astilectron.New(log.New(os.Stderr, "", 0), astilectron.Options{
		AppName:            "simpCal",
		AppIconDefaultPath: icon,
	})
	defer a.Close()

	// Start astilectron
	a.Start()

	// Create a new window
	var w, _ = a.NewWindow(serverURL, &astilectron.WindowOptions{
		Center:         astikit.BoolPtr(true),
		Fullscreenable: astikit.BoolPtr(true),
		Minimizable:    astikit.BoolPtr(true),
		Resizable:      astikit.BoolPtr(true),
		MinWidth:       astikit.IntPtr(1500),
		MinHeight:      astikit.IntPtr(1076),
		Width:          astikit.IntPtr(1920),
		Height:         astikit.IntPtr(1080),
		Icon:           &icon,
		
	})

	// Wait for server
	for _, err := http.Get(serverURL); err != nil; {
		time.Sleep(100 * time.Millisecond)
	}

	w.Create()
	if *debug {
		w.OpenDevTools()
	}

	// Blocking pattern
	a.Wait()

}
