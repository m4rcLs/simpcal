static_files_dir="../backend/resources/app"

cd frontend
yarn build
rm -rf $static_files_dir
mv dist $static_files_dir
cd ../backend
go build -ldflags -H=windowsgui -o simpCal.exe